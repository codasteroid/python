# How to upgrade Python on Ubuntu

Many linux distributions don't contain the latest version of Python installed by default, this can cause issues for developers who require a more recent version. Updating Python on Ubuntu can be challenging and may lead to problems, such as breaking the Gnome terminal. Before attempting to update Python on Ubuntu, it is important to carefully read and understand the instructions provided in the tutorial to avoid potential issues.

## Check which Python version is installed

Ubuntu 20.04 comes with Python 3.8.10 as default which is not the latest. To check which version of Python is installed on your Ubuntu system, open up your terminal with **ctrl + alt + t** and run the following command:

```bash
python3 --version
```
Example of the expected output:

```bash
Python 3.8.10
```
The output should provide information on which Python version is currently installed on your Ubuntu system.

## Install a new Python version

Since Ubuntu repositories don't include the latest version of Python as default, we can use an open-source package called [deadsnakes](https://github.com/deadsnakes) that provides old and recent Python versions packaged for Ubuntu. You can go along with the following steps to install a new Python package using **deadsnakes**.

First, update your local system repositories:

```bash
sudo apt update
```

Add the **deadsnakes** repository to your system APT's sources list:

```bash
sudo add-apt-repository ppa:deadsnakes/ppa
```

Then, check if the new version of Python that you want to install is available in the newly added repository:

```bash
apt list | grep python3.11
```
The output should list your preferred version. If so, you can install it by running the following command:

```bash
sudo apt install python3.11
```
When the new version of Python is installed, it does not automatically replace the default version that came with Ubuntu. Even after installation, checking the version of Python will still show the default version and not the new one that you just installed.

## Set up the newly installed Python version as the default

Before you set up the installed version as the default, you need to edit the configuration file of the Gnome terminal. Otherwise, your Gnome terminal will break. To avoid this problem, run the following command to start editing the configuration file:

```bash
sudo nano /usr/bin/gnome-terminal
```
Make sure the first line in the configuration file is pointing to the Python3 that exists in the `/bin` folder:

```bash
#!/usr/bin/python3
```
Change that line to be pointed to your current Python version (not the newly installed version) like below. Then, press **ctrl + x** and **y** for the changes to take effect.

```bash
#!/usr/bin/python3.8
```

Next, you can update the default Python by adding your newly installed Python version to the alternatives using the command below:

```bash
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.11 1
``` 
Now, check your Python version to make sure the installed version is set as the default.

## Reinstall the `python3-apt` and other dependencies

The current `python3-apt` package is not compatible with the current Python version, which will break `pip` and other packages like `disutils`. To fix this issue, remove first the current `python3-apt` package:

```bash
sudo apt remove --purge python3-apt
```
Then, reinstall the package (recommended to clean up before you reinstall using `sudo apt autoclean`):

```bash
sudo apt install python3-apt
```
Your current Python version doesn't have `disutils` package installed yet. To install it, run the following command and make sure the Python version matches your current Python:

```bash
sudo apt install python3.11-distutils
```
Next, you need to install a new `pip` for your current Python version. To do so, let's get the file and use it to install `pip`:

```bash
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
```
The `get-pip.py` is saved to your home directory. Execute the following command to install `pip`:

```bash
sudo python3.11 get-pip.py
```
Make sure the new `pip` is successfully installed by running:

```bash
pip --version
```
The output should provide information on which `pip` version is currently installed on your Ubuntu system.

Sometimes, you might get into an issue while creating a new virtual environment with `venv`. To fix this issue, you need to reinstall a new version of `venv` that is compatible with your current Python version:

```bash
sudo apt install python3.11-venv
```
Updating Python on Ubuntu can not only cause issues with the GNOME terminal, but it can also occasionally affect the GNOME desktop settings application. It is important to ensure that the settings application is still functioning properly. If not, you can install the Settings app through the terminal using the below command:

```bash
sudo apt install gnome-control-center  
```
Now, get back to the application menu and make sure the Settings app is installed and working.

Upgrading Python to a new version on Ubuntu is little bit complicated. Hopefully, this tutorial works for you and helps you learn and understand how to update your Python to a recent version on your Ubuntu system. If you have any questions, feel free to reach out to me on [LinkedIn](https://www.linkedin.com/in/rochdi-khalid/) at any time.
