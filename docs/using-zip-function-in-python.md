# How to use the `zip()` function in Python

Have you ever iterated over two or more lists at the same time in Python? If so, you may have found yourself writing nested loops or using alternative solutions to achieve the intended result. For example, one common approach is to use a nested `for` loop, which can quickly become difficult to understand, especially when dealing with multiple lists of different lengths.

Fortunately, Python has a built-in function called `zip()` that makes this task much easier by allowing you to combine multiple iterables (tuples, lists, etc) into a single iterable, where each element is a tuple containing one element from each of the input iterables.

Here is an example to help you understand how `zip()` works. Let's say you have two lists, one containing the names of fruits and another containing their corresponding quantities:

```python
fruits = ['orange', 'apple', 'peach']
quantities = [30, 70, 50]
```
You can use the `zip()` function to combine these two lists into a list of tuples:

```python
stock = zip(fruits, quantities) 
```
The `stock` variable contains the following list of tuples:

```bash
[('orange', 30), ('apple', 70), ('peach', 50)]
```
Then, you can iterate over this list of tuples using a `for` loop:

```python
for fruit, quantity in stock:
	print(fruit, quantity)
```
This will output the following:

```bash
orange 30
apple 70
peach 50
```
As seen, the `zip()` function makes it easy to iterate over multiple lists simultaneously, which can be useful in many different scenarios. Whether you are working on data sets, processing user inputs, or any other task that requires you to access elements from multiple iterables, you may be surprised at how much simpler and more intuitive your code becomes.

I hope you found this introduction to the `zip()` function in Python useful, give it a try and share it with others.